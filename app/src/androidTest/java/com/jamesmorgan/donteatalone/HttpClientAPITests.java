package com.jamesmorgan.donteatalone;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.jamesmorgan.donteatalone.api.BackendlessJsonObjectRequest;
import com.jamesmorgan.donteatalone.api.RestAPIClient;

import org.json.JSONException;
import org.json.JSONObject;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;

import static android.content.ContentValues.TAG;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Delacrix on 15/10/2016.
 */

@RunWith(AndroidJUnit4.class)
public class HttpClientAPITests {

    private final String YOUR_APP_ID = "65966741-7C06-4822-FFDF-E43D5DD98F00";
    private final String YOUR_SECRET_KEY = "783832B2-FAA8-CDE1-FFF9-D4CE9B97F700";
    private final String YOUR_APP_TYPE = "REST";
    private static final String USER_TOKEN = "0008BA5A-EDF6-7D27-FFFA-1C29FCACDB00";

    private static RequestQueue mQueue;

    @BeforeClass
    public static void setUpQueue() {
        RequestQueue queue = Volley.newRequestQueue(InstrumentationRegistry.getTargetContext());
        mQueue = queue;

        assertNotNull(mQueue);
    }

    private Request newRequest(int method, String url, JSONObject parameters, Response.Listener successListener, Response.ErrorListener errorListener) {
        BackendlessJsonObjectRequest request = new BackendlessJsonObjectRequest(method, url, parameters, successListener, errorListener);

        if (USER_TOKEN != null) {
            // request.putHeader("user-token", USER_TOKEN);
            request.putHeader("application-id", YOUR_APP_ID);
            request.putHeader("secret-key", YOUR_SECRET_KEY);
            request.putHeader("application-type", YOUR_APP_TYPE);
        }

        return request;
    }

    @Test
    public void makeReport() throws Exception {
        final CountDownLatch signal = new CountDownLatch(1);

        final String url = "https://api.backendless.com/v1/data/Report";

        String user = "Jocelyn";
        String category = "FFK";
        String remark = "What the hell bro?";

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("user_objectid", USER_TOKEN);
            parameters.put("reported_user", user);
            parameters.put("reported_category", category);
            parameters.put("reported_remark", remark);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = newRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                signal.countDown();
                Assert.assertNotNull(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                signal.countDown();
                assertNull(error);
            }
        });

        mQueue.add(request);
        signal.await();
    }
}
