package com.jamesmorgan.donteatalone.controller;

import com.jamesmorgan.donteatalone.model.User;

/**
 * Created by Delacrix on 08/10/2016.
 */

public class UserController {

    private static UserController sUserController;

    private User mLoggedInUser;

    private UserController() {

    }

    public static synchronized UserController getInstance() {
        if (sUserController == null) {
            sUserController = new UserController();
        }

        return sUserController;
    }

    public User getLoggedInUser() {
        return mLoggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        mLoggedInUser = loggedInUser;
    }

}
