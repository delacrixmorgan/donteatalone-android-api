package com.jamesmorgan.donteatalone.model;

/**
 * Created by Delacrix on 29/09/2016.
 */

public class Question {
    private String mFirstChoice, mSecondChoice;

    public Question(String mFirstChoice, String mSecondChoice) {
        this.mFirstChoice = mFirstChoice;
        this.mSecondChoice = mSecondChoice;
    }

    public String getmFirstChoice() {
        return mFirstChoice;
    }

    public void setmFirstChoice(String mFirstChoice) {
        this.mFirstChoice = mFirstChoice;
    }

    public String getmSecondChoice() {
        return mSecondChoice;
    }

    public void setmSecondChoice(String mSecondChoice) {
        this.mSecondChoice = mSecondChoice;
    }
}

