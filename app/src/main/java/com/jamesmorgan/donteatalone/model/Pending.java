package com.jamesmorgan.donteatalone.model;

import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Delacrix on 13/10/2016.
 */

public class Pending {
    private String mMeetupType;
    private String mAnswer;
    private String mUserObjectID;
    private Boolean mAvailable;

    public Pending(@NonNull JSONObject jsonObject) throws Exception {
        mMeetupType = jsonObject.optString("meeting_type");
        mAnswer = jsonObject.optString("answer");
        mUserObjectID = jsonObject.optString("user_objectid");
        mAvailable = jsonObject.optBoolean("available");
    }

    public Pending(String mMeetupType, String mAnswer, String mUserObjectID, Boolean mAvailable) {
        this.mMeetupType = mMeetupType;
        this.mAnswer = mAnswer;
        this.mUserObjectID = mUserObjectID;
        this.mAvailable = mAvailable;
    }

    public String getmMeetupType() {
        return mMeetupType;
    }

    public void setmMeetupType(String mMeetupType) {
        this.mMeetupType = mMeetupType;
    }

    public String getmAnswer() {
        return mAnswer;
    }

    public void setmAnswer(String mAnswer) {
        this.mAnswer = mAnswer;
    }

    public String getmUserObjectID() {
        return mUserObjectID;
    }

    public void setmUserObjectID(String mUserObjectID) {
        this.mUserObjectID = mUserObjectID;
    }

    public Boolean getmAvailable() {
        return mAvailable;
    }

    public void setmAvailable(Boolean mAvailable) {
        this.mAvailable = mAvailable;
    }
}
