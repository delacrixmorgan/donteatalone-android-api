package com.jamesmorgan.donteatalone.model;

import java.sql.Time;

/**
 * Created by Delacrix on 29/09/2016.
 */

public class Meetup {
    private User mUser[] = new User[4];
    private Location myLocation;
    private String mMeetingTime;

    public Meetup(User[] mUser, Location myLocation, String mMeetingTime) {
        this.mUser = mUser;
        this.myLocation = myLocation;
        this.mMeetingTime = mMeetingTime;
    }

    public User[] getmUser() {
        return mUser;
    }

    public void setmUser(User[] mUser) {
        this.mUser = mUser;
    }

    public Location getMyLocation() {
        return myLocation;
    }

    public void setMyLocation(Location myLocation) {
        this.myLocation = myLocation;
    }

    public String getmMeetingTime() {
        return mMeetingTime;
    }

    public void setmMeetingTime(String mMeetingTime) {
        this.mMeetingTime = mMeetingTime;
    }

}
