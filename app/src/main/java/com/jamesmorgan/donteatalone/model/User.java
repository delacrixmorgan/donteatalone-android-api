package com.jamesmorgan.donteatalone.model;

import android.support.annotation.NonNull;

import org.json.JSONObject;

/**
 * Created by Delacrix on 29/09/2016.
 */

public class User {
    private String mId;
    private String mUsername;
    private String mEmail;
    private String mPassword;
    private String mUserToken;
    private String mObjectID;

    public User(String mUsername, String mEmail, String mPassword, String mUserToken, String mObjectID) {
        this.mUsername = mUsername;
        this.mEmail = mEmail;
        this.mPassword = mPassword;
        this.mUserToken = mUserToken;
        this.mObjectID = mObjectID;
    }

    public User(@NonNull JSONObject jsonObject) throws Exception {
        mId = jsonObject.optString("ownerId");
        mUsername = jsonObject.optString("username");
        mEmail = jsonObject.optString("email");
        mPassword = jsonObject.optString("password");
        mUserToken = jsonObject.optString("user-token");
        mObjectID = jsonObject.optString("objectId");
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmUsername() {
        return mUsername;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getmUserToken() {
        return mUserToken;
    }

    public void setmUserToken(String mUserToken) {
        this.mUserToken = mUserToken;
    }

    public String getmObjectID() {
        return mObjectID;
    }

    public void setmObjectID(String mObjectID) {
        this.mObjectID = mObjectID;
    }

    @Override
    public String toString() {
        return mUsername;
    }
}
