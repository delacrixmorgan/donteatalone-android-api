package com.jamesmorgan.donteatalone.model;

/**
 * Created by Delacrix on 29/09/2016.
 */

public class Report {
    private String mUsername, mDescription;

    public Report(String mUsername, String mDescription) {
        this.mUsername = mUsername;
        this.mDescription = mDescription;
    }

    public String getmUsername() {
        return mUsername;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }
}
