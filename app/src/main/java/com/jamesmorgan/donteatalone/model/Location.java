package com.jamesmorgan.donteatalone.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Delacrix on 29/09/2016.
 */

public class Location {
    private String mName;
    private LatLng mCoordinates;

    public Location(String mName, LatLng mCoordinates) {
        this.mName = mName;
        this.mCoordinates = mCoordinates;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public LatLng getmCoordinates() {
        return mCoordinates;
    }

    public void setmCoordinates(LatLng mCoordinates) {
        this.mCoordinates = mCoordinates;
    }
}
