package com.jamesmorgan.donteatalone;

import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.jamesmorgan.donteatalone.api.RestAPIClient;
import com.jamesmorgan.donteatalone.ui.AboutFragment;
import com.jamesmorgan.donteatalone.ui.HistoryFragment;
import com.jamesmorgan.donteatalone.ui.ProfileFragment;
import com.jamesmorgan.donteatalone.ui.ReportFragment;
import com.jamesmorgan.donteatalone.ui.request.RequestFragment;
import com.jamesmorgan.donteatalone.ui.SettingFragment;
import com.jamesmorgan.donteatalone.ui.SignupFragment;
import com.jamesmorgan.donteatalone.ui.WebFragment;

import static android.content.Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT;
import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_REORDER_TO_FRONT;

public class DrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        mDrawerLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.app_bar, null);
        mNavigationView = (NavigationView) mDrawerLayout.findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        getLayoutInflater().inflate(layoutResID, mNavigationView, true);

        super.setContentView(mDrawerLayout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        setSupportActionBar(toolbar);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator(R.drawable.hamburger_icon);;
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setTitle(R.string.app_name);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment targetFragment = null;

        switch (item.getItemId()) {
            case R.id.nav_profile:
                if (RestAPIClient.getInstance(getApplicationContext()).getSharedPreference().getString("USER_TOKEN", null) == null){
                    Intent intent = new Intent(getApplicationContext(), AccountActivity.class);
                    intent.setFlags(FLAG_ACTIVITY_BROUGHT_TO_FRONT | FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    mDrawerLayout.closeDrawers();
                    return true;
                } else {
                    targetFragment = new ProfileFragment();
                }

                break;

            case R.id.nav_request:
                targetFragment = new RequestFragment();
                break;

            case R.id.nav_history:
                targetFragment = new HistoryFragment();
                break;

            case R.id.nav_report:
                targetFragment = new ReportFragment();
                break;

            case R.id.nav_settings:
                targetFragment = new SettingFragment();
                break;

            case R.id.nav_about:
                targetFragment = new AboutFragment();
                break;
        }

        if (targetFragment == null) {
            return true;
        }

        Fragment existingFragment = getSupportFragmentManager().findFragmentById(R.id.activity_main_vg_fragment);
        if (existingFragment == null || existingFragment.getClass() != targetFragment.getClass()) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.activity_main_vg_fragment, targetFragment)
                    .addToBackStack(targetFragment.getClass().getSimpleName())
                    .commit();
        }

        mDrawerLayout.closeDrawers();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }

}
