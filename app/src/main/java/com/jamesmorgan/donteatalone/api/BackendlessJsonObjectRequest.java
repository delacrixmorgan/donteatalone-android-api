package com.jamesmorgan.donteatalone.api;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Delacrix on 08/10/2016.
 */

public class BackendlessJsonObjectRequest extends JsonObjectRequest {

    private final String YOUR_APP_ID = "65966741-7C06-4822-FFDF-E43D5DD98F00";
    private final String YOUR_SECRET_KEY = "783832B2-FAA8-CDE1-FFF9-D4CE9B97F700";
    private final String YOUR_APP_TYPE = "REST";

    private final Map<String, String> mAdditionalHeaders = new HashMap<>();

    public BackendlessJsonObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public void putHeader(String key, String value) {
        mAdditionalHeaders.put(key, value);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>(super.getHeaders());

        headers.put("application-id", YOUR_APP_ID);
        headers.put("secret-key", YOUR_SECRET_KEY);
        headers.put("application-type", YOUR_APP_TYPE);

        headers.putAll(mAdditionalHeaders);

        return headers;
    }
}
