package com.jamesmorgan.donteatalone.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.jamesmorgan.donteatalone.controller.UserController;
import com.jamesmorgan.donteatalone.model.Pending;
import com.jamesmorgan.donteatalone.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * Created by Delacrix on 06/10/2016.
 */

public class RestAPIClient {

    private static RestAPIClient sSharedInstance;

    private RequestQueue mRequestQueue;
    private String mUserToken;
    private String mObjectID;
    private SharedPreferences mSharedPreferences;

    private final String BASE_URL = "https://api.backendless.com/v1/";
    private final String USER_TOKEN = "USER_TOKEN";
    private final String USER_OBJECT_ID = "USER_OBJECT_ID";

    // Constructor
    private RestAPIClient(@NonNull Context context) {
        // We'll use this RequestQueue to dispatch our requests
        mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        mSharedPreferences = context.getSharedPreferences("preference-key", Context.MODE_PRIVATE);
        mUserToken = loadUserToken();
    }

    // Singleton Instances
    public static synchronized RestAPIClient getInstance(@NonNull Context context) {
        if (sSharedInstance == null) {
            sSharedInstance = new RestAPIClient(context);
        }

        return sSharedInstance;
    }

    private Request newRequest(int method, String url, JSONObject parameters, Response.Listener successListener, Response.ErrorListener errorListener) {
        BackendlessJsonObjectRequest request = new BackendlessJsonObjectRequest(method, url, parameters, successListener, errorListener);

        if (mUserToken != null) {
            request.putHeader("user-token", mUserToken);
        }

        return request;
    }

    // Interfaces

    public interface OnUserAuthenticationCompleteListener {
        void onComplete(User user, VolleyError error);
    }

    public interface OnRequestAuthenticationCompleteListener {
        void onComplete(ArrayList<Pending> pendings, VolleyError error);
    }


    // API Method Calling

    public void signUp(User user, final OnUserAuthenticationCompleteListener completionListener) {
        String url = BASE_URL + "data/Users";

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("username", user.getmUsername());
            parameters.put("email", user.getmEmail());
            parameters.put("password", user.getmPassword());
            parameters.put("token", user.getmUserToken());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = newRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    User user = new User(response);

                    saveUserToken(user.getmUserToken());
                    saveUserObjectID(user.getmObjectID());

                    UserController.getInstance().setLoggedInUser(user);

                    completionListener.onComplete(user, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                completionListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

    public void login(String email, String password, final OnUserAuthenticationCompleteListener completionListener) {
        String url = BASE_URL + "users/login";

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("login", email);
            parameters.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = newRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    User user = new User(response);

                    saveUserToken(user.getmUserToken());
                    saveUserObjectID(user.getmObjectID());

                    UserController.getInstance().setLoggedInUser(user);

                    completionListener.onComplete(user, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                completionListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

    public void userDetails(String userObjectID, final OnUserAuthenticationCompleteListener completionListener) {
        String url = BASE_URL + "data/Users/" + userObjectID;

        Request request = newRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    User user = new User(response);

                    completionListener.onComplete(user, null);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                completionListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

    public void makePairRequest(String answer, final OnUserAuthenticationCompleteListener completionListener) {
        String url = BASE_URL + "data/Request";

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("user_objectid", loadUserObjectID());
            parameters.put("meeting_type", "pair");
            parameters.put("answer", answer);
            parameters.put("available", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = newRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    completionListener.onComplete(null, null);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                completionListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

    public void checkPairRequest(String answer, final OnRequestAuthenticationCompleteListener completionListener) {
        String url = BASE_URL + "data/Request?where=answer%20%3D%20%27" + answer + "%27";

        Request request = newRequest(Request.Method.GET, url, null , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    ArrayList<Pending> pendings = new ArrayList<>();

                    JSONArray dataArray = response.optJSONArray("data");
                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject dataJson = dataArray.optJSONObject(i);
                        try {
                            Pending pending = new Pending(dataJson);

                            if(!pending.getmUserObjectID().equals(loadUserObjectID())){
                                pendings.add(pending);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    completionListener.onComplete(pendings, null);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                completionListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }


    // Shared Preference

    public SharedPreferences getSharedPreference() {
        if (mSharedPreferences != null) {
            return mSharedPreferences;
        } else {
            return null;
        }
    }

    // Tokenisation

    private void saveUserToken(String token) {
        mUserToken = token;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_TOKEN, token);
        editor.apply();
    }

    private String loadUserToken() {
        return mSharedPreferences.getString(USER_TOKEN, null);
    }

    private void saveUserObjectID(String objectID) {
        mObjectID = objectID;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_OBJECT_ID, objectID);
        editor.apply();
    }

    public String loadUserObjectID() {
        return mSharedPreferences.getString(USER_OBJECT_ID, null);
    }


}
