package com.jamesmorgan.donteatalone.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamesmorgan.donteatalone.R;
import com.jamesmorgan.donteatalone.ui.HistoryViewHolder;

/**
 * Created by Delacrix on 01/10/2016.
 */

public class HistoryListAdapter extends RecyclerView.Adapter {

    private HistoryViewHolder.OnHistorySelectedListener mHistoryListener;

    @Override
    public int getItemCount() {
        return 100;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.activity_history_view, parent, false);

        return new HistoryViewHolder(view, mHistoryListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    }

    public HistoryViewHolder.OnHistorySelectedListener getHistoryListener() {
        return mHistoryListener;
    }

    public void setHistoryListener(HistoryViewHolder.OnHistorySelectedListener historyListener) {
        this.mHistoryListener = historyListener;
    }
}
