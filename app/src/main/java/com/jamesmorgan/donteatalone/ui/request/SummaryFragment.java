package com.jamesmorgan.donteatalone.ui.request;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jamesmorgan.donteatalone.R;

/**
 * Summary of Order
 * Created by Don't Eat Alone on 9/7/2016.
 */
public class SummaryFragment extends Fragment{
    private Button mFinishButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_summary, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mFinishButton = (Button) view.findViewById(R.id.fragment_summary_btn_finish);
        mFinishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        super.onViewCreated(view, savedInstanceState);
    }
}
