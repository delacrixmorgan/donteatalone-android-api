package com.jamesmorgan.donteatalone.ui.request;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.jamesmorgan.donteatalone.R;
import com.jamesmorgan.donteatalone.api.RestAPIClient;
import com.jamesmorgan.donteatalone.model.Pending;
import com.jamesmorgan.donteatalone.model.User;

import java.util.ArrayList;

/**
 * Created by Delacrix on 13/10/2016.
 */

public class WaitingFragment extends Fragment {

    private Button mRefreshButton, mCancelButton;
    private ArrayList<Pending> mPending;
    private TextView mRequestName, mRequestStatus;
    private int mCurrentRequest = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_waiting, container, false);

        mRequestName = (TextView) rootView.findViewById(R.id.request_name);
        mRequestStatus = (TextView) rootView.findViewById(R.id.request_status);
        mRefreshButton = (Button) rootView.findViewById(R.id.check_request_btn);
        mCancelButton = (Button) rootView.findViewById(R.id.cancel_request_btn);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPending != null) {
                    if(mRefreshButton.getText().equals("Accept")){
                        Toast.makeText(getContext(), "Accepted Request", Toast.LENGTH_SHORT).show();
                        WaitingFragment.this.showSummaryFragment();
                    }
                    displayRequest();
                } else {
                    apiCheckRequest("B");
                }
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentRequest++;
                displayRequest();
            }
        });
    }

    public void showSummaryFragment() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_fragment, new SummaryFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack("WaitingFragment")
                .commit();
    }

    public void apiCheckRequest(String answer) {
        RestAPIClient.getInstance(getContext()).checkPairRequest(answer, new RestAPIClient.OnRequestAuthenticationCompleteListener() {
            @Override
            public void onComplete(ArrayList<Pending> pendings, VolleyError error) {
                if (error != null) {
                    Snackbar.make(getView(), error.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
                } else {
                    mPending = pendings;
                }
            }
        });
    }

    public void displayRequest() {
        final ProgressDialog pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.show();

        RestAPIClient.getInstance(getContext()).userDetails(mPending.get(mCurrentRequest).getmUserObjectID(), new RestAPIClient.OnUserAuthenticationCompleteListener() {
            @Override
            public void onComplete(User user, VolleyError error) {
                if (error != null) {
                    pDialog.hide();
                    Snackbar.make(getView(), error.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();

                } else {
                    pDialog.hide();
                    mRequestName.setText(user.getmUsername());
                    mRefreshButton.setText("Accept");
                }
            }
        });
    }
}
