package com.jamesmorgan.donteatalone.ui.request;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.jamesmorgan.donteatalone.R;
import com.jamesmorgan.donteatalone.api.RestAPIClient;
import com.jamesmorgan.donteatalone.model.Question;
import com.jamesmorgan.donteatalone.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

/**
 * Group users according to a series of random questions
 * Created by Don't Eat Alone on 9/7/2016.
 */
public class QuestionFragment extends Fragment {
    private ArrayList<Question> mQuestion = new ArrayList<>();
    private Button mFirstAnswerButton;
    private Button mSecondAnswerButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_question, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mFirstAnswerButton = (Button) view.findViewById(R.id.fragment_question_btn_first_answer);
        mSecondAnswerButton = (Button) view.findViewById(R.id.fragment_question_btn_second_answer);

        mFirstAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeAPIRequest("A");
            }
        });
        mSecondAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeAPIRequest("B");
            }
        });

        populateQuestions();
        randomiseQuestion();

        super.onViewCreated(view, savedInstanceState);
    }

    public void makeAPIRequest(String answer){

        RestAPIClient.getInstance(getContext()).makePairRequest(answer, new RestAPIClient.OnUserAuthenticationCompleteListener() {
            @Override
            public void onComplete(User user, VolleyError error) {
                if (error != null) {
                    Snackbar.make(getView(), error.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();

                } else {
                    Toast.makeText(getContext(), "Request Sent", Toast.LENGTH_SHORT).show();
                    QuestionFragment.this.showWaitingFragment();
                }
            }
        });
    }

    public void showWaitingFragment() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_fragment, new WaitingFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack("QuestionFragment")
                .commit();
    }

    public void randomiseQuestion(){
        Question selectedQuestion = mQuestion.get(new Random().nextInt(mQuestion.size()));

        mFirstAnswerButton.setText(selectedQuestion.getmFirstChoice());
        mSecondAnswerButton.setText(selectedQuestion.getmSecondChoice());

    }

    public StringBuilder loadJSONFromAsset() {

        StringBuilder buf = new StringBuilder();
        InputStream json = null;
        try {
            json = getActivity().getAssets().open("question.json");
            BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
            String str;

            while ((str = in.readLine()) != null) {
                buf.append(str);
            }

            in.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return buf;
    }

    public void populateQuestions() {
        try {
            JSONObject jsonRootObject = new JSONObject(String.valueOf(loadJSONFromAsset()));

            //Get the instance of JSONArray that contains JSONObjects
            JSONArray jsonArray = jsonRootObject.optJSONArray("Question");

            //Iterate the jsonArray and print the info of JSONObjects
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                mQuestion.add(new Question(jsonObject.optString("A"), jsonObject.optString("B")));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
