package com.jamesmorgan.donteatalone.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.jamesmorgan.donteatalone.R;
import com.jamesmorgan.donteatalone.api.RestAPIClient;
import com.jamesmorgan.donteatalone.model.User;

import static android.content.ContentValues.TAG;

/**
 * Created by Delacrix on 11/10/2016.
 */

public class ProfileFragment extends Fragment {
    private TextView mUsername;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        mUsername = (TextView) rootView.findViewById(R.id.profile_username);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RestAPIClient.getInstance(getContext()).userDetails(RestAPIClient.getInstance(getContext()).loadUserObjectID(), new RestAPIClient.OnUserAuthenticationCompleteListener() {
            @Override
            public void onComplete(User user, VolleyError error) {
                if (error != null) {
                    Snackbar.make(getView(), error.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();

                } else {
                    mUsername.setText(user.getmUsername());
                }
            }
        });
    }
}
