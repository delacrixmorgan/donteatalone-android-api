package com.jamesmorgan.donteatalone.ui.history;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.jamesmorgan.donteatalone.R;
import com.jamesmorgan.donteatalone.model.Meetup;

/**
 * Created by Delacrix on 01/10/2016.
 */

public class HistoryViewHolder extends RecyclerView.ViewHolder {

    private TextView userName, locationName;
    private Meetup mMeetup;

    public interface OnHistorySelectedListener{
        void onHistorySelected(Meetup meetup);
    }

    public HistoryViewHolder(View itemView, final OnHistorySelectedListener handler) {
        super(itemView);

        userName = (TextView) itemView.findViewById(R.id.history_username);
        locationName = (TextView) itemView.findViewById(R.id.history_location_name);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.onHistorySelected(mMeetup);
            }
        });
    }

    public void setHistoryEntry(@NonNull Meetup history){
        mMeetup = history;
        userName.setText(history.getmUser().toString());
        locationName.setText(history.getMyLocation().toString());
    }

}
