package com.jamesmorgan.donteatalone.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.jamesmorgan.donteatalone.R;
import com.jamesmorgan.donteatalone.api.RestAPIClient;
import com.jamesmorgan.donteatalone.model.User;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.content.ContentValues.TAG;

/**
 * Created by Delacrix on 10/09/16.
 */
public class SignupFragment extends Fragment {

    private EditText mUsername, mEmail, mPassword;
    private Button mFacebookLoginButton, mSignupButton;
    private TextView mEmailLoginText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_signup, container, false);

        mUsername = (EditText) rootView.findViewById(R.id.account_fragment_username);
        mEmail = (EditText) rootView.findViewById(R.id.account_fragment_email);
        mPassword = (EditText) rootView.findViewById(R.id.account_fragment_password);

        mFacebookLoginButton = (Button) rootView.findViewById(R.id.account_fragment_btn_facebook_login);
        mSignupButton = (Button) rootView.findViewById(R.id.account_fragment_btn_signup);
        mEmailLoginText = (TextView) rootView.findViewById(R.id.account_fragment_btn_email_login);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mFacebookLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignupFragment.this.showRequestFragment();
            }
        });

        mSignupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SignupFragment.this.apiSignUp();
                } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
                    Log.d(TAG, "onClick: " + e.getMessage());
                }
            }
        });

        mEmailLoginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignupFragment.this.showLoginFragment();
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    public void showRequestFragment() {
        getActivity().finish();
    }

    public void apiSignUp() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String entryUsername = mUsername.getText().toString();
        String entryEmail = mEmail.getText().toString();
        String entryPassword = mPassword.getText().toString();
        //hashUserPassword(mPassword.getText().toString());
        String entryUserToken = generateUserToken();

        final ProgressDialog pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.show();

        User user = new User(entryUsername, entryEmail, entryPassword, entryUserToken, "123");

        RestAPIClient.getInstance(getContext()).signUp(user, new RestAPIClient.OnUserAuthenticationCompleteListener() {
            @Override
            public void onComplete(User user, VolleyError error) {
                if (error != null) {
                    Snackbar.make(getView(), error.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
                    pDialog.hide();
                } else {
                    Toast.makeText(getContext(), "Successfully Signup", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                    pDialog.hide();
                }
            }
        });
    }

    public String hashUserPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes("UTF-8"));

        return md.digest().toString();
    }

    public String generateUserToken() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String currentDateTime = Calendar.getInstance().toString();
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(currentDateTime.getBytes("UTF-8"));

        return md.digest().toString();
    }

    public void showLoginFragment() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_vg_fragment, new LoginFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack("SignupFragment")
                .commit();
    }
}
