package com.jamesmorgan.donteatalone.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.jamesmorgan.donteatalone.R;
import com.jamesmorgan.donteatalone.api.RestAPIClient;
import com.jamesmorgan.donteatalone.model.User;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 * Login Fragment for
 * Created by Don't Eat Alone on 9/7/2016.
 */
public class LoginFragment extends Fragment {

    private EditText mEmail, mPassword;
    private Button mLoginButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        mEmail = (EditText) rootView.findViewById(R.id.login_email);
        mPassword = (EditText) rootView.findViewById(R.id.login_password);

        mLoginButton = (Button) rootView.findViewById(R.id.login_button);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    LoginFragment.this.apiLogin();
                } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void apiLogin() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String entryEmail = mEmail.getText().toString();
        String entryPassword = mPassword.getText().toString();
        // hashUserPassword(mPassword.getText().toString());

        final ProgressDialog pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.show();

        RestAPIClient.getInstance(getContext()).login(entryEmail, entryPassword, new RestAPIClient.OnUserAuthenticationCompleteListener() {
            @Override
            public void onComplete(User user, VolleyError error) {
                if(error != null){
                    Snackbar.make(getView(), error.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
                    pDialog.hide();
                } else {
                    Toast.makeText(getContext(), "Successfully Login", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                    pDialog.hide();
                }
            }
        });
    }

    public String hashUserPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes("UTF-8"));

        return md.digest().toString();
    }
}
