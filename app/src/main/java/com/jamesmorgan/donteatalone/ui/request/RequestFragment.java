package com.jamesmorgan.donteatalone.ui.request;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.jamesmorgan.donteatalone.R;

/**
 * Created by Delacrix on 01/10/2016.
 */

public class RequestFragment extends Fragment {
    private LinearLayout mRequestLayout;
    private Button mGroupOptionButton;
    private Button mPairOptionButton;

    private static final String TAG = "RequestFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_request, container, false);
        mRequestLayout = (LinearLayout) rootView.findViewById(R.id.request_layout);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mGroupOptionButton = (Button) view.findViewById(R.id.fragment_request_btn_group);
        mPairOptionButton = (Button) view.findViewById(R.id.fragment_request_btn_pair);

        mGroupOptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestFragment.this.showQuestionFragment();
            }
        });

        mPairOptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestFragment.this.showQuestionFragment();
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    public void showQuestionFragment() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null)
                .replace(R.id.activity_main_vg_fragment, new QuestionFragment())
                .commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: showing on screen");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: dismissing on screen");
    }

}
