package com.jamesmorgan.donteatalone.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.jamesmorgan.donteatalone.R;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

/**
 * Created by Delacrix on 01/10/2016.
 */

public class ReportFragment extends Fragment{

    private static final String[] REPORT_TYPES = {"FFK", "Bailed When Paying Bill", "Profile Picture is Deceiving", "Other Nefarious Reason"};
    private MaterialBetterSpinner mSpinner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_report, container, false);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, REPORT_TYPES);

        mSpinner = (MaterialBetterSpinner) rootView.findViewById(R.id.report_type);
        mSpinner.setAdapter(arrayAdapter);


        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
