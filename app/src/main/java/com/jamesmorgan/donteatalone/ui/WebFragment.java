package com.jamesmorgan.donteatalone.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.jamesmorgan.donteatalone.R;
import com.jamesmorgan.donteatalone.api.RestAPIClient;

import org.json.JSONArray;

import static android.content.ContentValues.TAG;

/**
 * Created by Delacrix on 06/10/2016.
 */

public class WebFragment extends Fragment {

    private EditText mUrlName;
    private Button mLoadButton;
    private TextView mWebCode;
    private ProgressBar mProgressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_web, container, false);

        mUrlName = (EditText) rootView.findViewById(R.id.url_name);
        mLoadButton = (Button) rootView.findViewById(R.id.load_button);
        mWebCode = (TextView) rootView.findViewById(R.id.web_page_code);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mLoadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCode();
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    public void requestCode() {
        String url = "http://api.androidhive.info/volley/person_array.json";

        final ProgressDialog pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.show();

        mProgressBar.setIndeterminate(true);
        mProgressBar.setAlpha(1);

        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        mWebCode.setText(response.toString());
                        mProgressBar.setIndeterminate(false);
                        mProgressBar.setAlpha(0);
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                mProgressBar.setIndeterminate(false);
                mProgressBar.setAlpha(0);
                pDialog.hide();
            }
        });

        //RestAPIClient.getInstance(getContext()).addToRequestQueue(req);

    }
}
