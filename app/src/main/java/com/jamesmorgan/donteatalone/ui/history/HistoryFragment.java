package com.jamesmorgan.donteatalone.ui.history;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamesmorgan.donteatalone.R;
import com.jamesmorgan.donteatalone.adapter.HistoryListAdapter;
import com.jamesmorgan.donteatalone.model.Meetup;
import com.jamesmorgan.donteatalone.model.User;

import java.util.ArrayList;

/**
 * Created by Delacrix on 29/09/2016.
 */

public class HistoryFragment extends Fragment implements HistoryViewHolder.OnHistorySelectedListener{

    private String[] listUsername = {"Jocelyn", "Carene", "Laura"};
    private String[] listEmail = new String[]{"jocelyn@gmail.com", "carene@gmail.com", "laura@gmail.com"};

    private RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);

        ArrayList<User> userArrayList = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            userArrayList.add(new User(listUsername[i], listEmail[i], "password", "profilepicture.png", "123"));
        }

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.user_list);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Layout Manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);

        // History List Adapter
        HistoryListAdapter mHistoryListAdapter = new HistoryListAdapter();
        mHistoryListAdapter.setHistoryListener((com.jamesmorgan.donteatalone.ui.HistoryViewHolder.OnHistorySelectedListener) this);

        mRecyclerView.setAdapter(mHistoryListAdapter);
    }

    @Override
    public void onHistorySelected(Meetup meetup) {
        Snackbar.make(getView(), "Good Mythical Morning", Snackbar.LENGTH_SHORT).show();
    }
}
