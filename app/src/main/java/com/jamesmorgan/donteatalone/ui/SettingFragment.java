package com.jamesmorgan.donteatalone.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.jamesmorgan.donteatalone.AccountActivity;
import com.jamesmorgan.donteatalone.R;
import com.jamesmorgan.donteatalone.api.RestAPIClient;

import static android.content.Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT;
import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

/**
 * Created by Delacrix on 01/10/2016.
 */

public class SettingFragment extends PreferenceFragmentCompat {

    private Preference mUsername, mNotifcation, mSound, mSignout;

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        setPreferencesFromResource(R.xml.preferences, s);

        mUsername = findPreference("username");
        mNotifcation = findPreference("notification");
        mSound = findPreference("sound");
        mSignout = findPreference("sign_out");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mUsername.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                return true;
            }
        });

        mNotifcation.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                return true;
            }
        });

        mSound.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                return true;
            }
        });

        mSignout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                SharedPreferences mySPrefs = RestAPIClient.getInstance(getContext()).getSharedPreference();
                SharedPreferences.Editor editor = mySPrefs.edit();
                editor.remove("USER_TOKEN");
                editor.apply();

                Intent intent = new Intent(getContext(), AccountActivity.class);
                intent.setFlags(FLAG_ACTIVITY_BROUGHT_TO_FRONT | FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                Toast.makeText(getContext(), "Successfully Logout", Toast.LENGTH_SHORT).show();

                return true;
            }
        });

    }
}